import { Component, OnInit } from '@angular/core';
import { Users } from '../../mock-users'
import { User } from '../user';
import { FirestoreAPIService } from '../../core/firestore-api.service'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users = Users;
  selectedUser:User;

  constructor(private api: FirestoreAPIService) { }

  ngOnInit() {
    this.getUsers();
  }

  onSelect(selectedUser:User) {
    this.selectedUser = selectedUser;
    console.log(selectedUser.name);
  }

  getUsers() {
    this.api.getUsers()
      .subscribe(users => this.users = users);
  }

}
