import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'
// import { FirebaseAuthState } from '@angular/fire/auth'
import { from, Observable } from 'rxjs';
import { auth, User } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  appAuthState: Observable<User> = null;

  constructor(public afAuth: AngularFireAuth) {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
   }

  googleLogin() {
    const provider = new auth.GoogleAuthProvider()
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user)
      })
  }

  private updateUserData(user) {
    // Sets user data to firestore on login

    // const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);

    // const data: User = {
    //   uid: user.uid,
    //   email: user.email,
    //   displayName: user.displayName,
    //   photoURL: user.photoURL
    // }

    // return userRef.set(data, { merge: true })

  }


  signOut() {
    this.afAuth.auth.signOut().then(() => {
        // this.router.navigate(['/']);
    });
  }

   get authenticated(): boolean {
     this.appAuthState = this.afAuth.authState;
     return (this.appAuthState == null) ? false : true;
   }

   get currentUser(): any {
     return this.afAuth.auth.currentUser;
   }

   get currentUserId(): String {
     return this.afAuth.auth.currentUser.uid;
   }

   private socialSignIn

}
