import { Injectable } from '@angular/core';
import { User } from '../user/user';
import { Users } from '../mock-users'
import { Observable, of, Subscriber, from } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirestoreAPIService {

  userCollection: AngularFirestoreCollection<User>
  users: AngularFirestoreCollection<User>;

  db: AngularFirestore;

  constructor(db: AngularFirestore) { this.db = db; }

  getUsers(): Observable<User[]> {
    this.users = this.db.collection("users");
    
    // return of(Users);

    // return this.users.valueChanges;
    
  }
}
